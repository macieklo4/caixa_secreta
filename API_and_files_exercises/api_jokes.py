import requests


def get_joke():
    random_joke_all = requests.get('https://icanhazdadjoke.com/slack').json()
    random_joke_dict = random_joke_all['attachments']
    random_joke = random_joke_dict[0]['text']
    return random_joke

def letsplay():
    path  = '/home/pipr/zadania/jokes'
    random_joke = get_joke()
    question = input(f'Does this appeal to you...{random_joke}\n')
    if question == 'Yes':
        with open(path,'a+') as file:
            file.write(f'{random_joke}\n')
    else:
        return 'Maybe next time :)'

letsplay()                

    


