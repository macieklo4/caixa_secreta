import json
import csv

class People:
    def __init__(self,id,name,sex,birth_date):
        self.id = id
        self.name = name
        self.sex = sex
        self.birth_date = birth_date

class File:
    def __init__(self,file):
        self.file = file
         
    def name(self):
        with open(self.file,'r') as f:
            return f.name

    # def directory(self):
    #     with open(self.file,'r') as f:
    #         return f.directory

    def lines(self):
        i = 0
        with open(self.file,'r') as f:
            for line in f:
                i+=1
        return i     

    def create_objects(self):
        people = []
        with open(self.file) as f:
            f.readline()
            for line in f:
                line = line.rstrip()
                tokens = line.split(',')
                id,name,sex,birth_date = tokens
                person = People(id,name,sex,birth_date)
                people.append(person)
        return people    

def csvread(path):
    with open(path,'r') as file:
        csv_reader = csv.DictReader(file)
        result = []
        for person in people:
            result.append(People(
                person['id'],
                person['name'],
                person['sex'],
                person['birth_date']
            ))
        return result    


def csvwrite(json,path):
    with open(json,'r') as f:
        for line in f:
            pass
        with open(path,'w') as file:
            fieldnames = ['id', 'name', 'sex','birth_date']
            csv_writer = csv.DictWriter(file,fieldnames=fieldnames)
            csv_writer.writeheader()
            for person in f:
                list= [
                    person['id'],
                    person['name'],
                    person['sex'],
                    person['birth_date']
                ]
                csv_writer.writerow(list)

def jsonread(path):
    with open(path, 'r') as file:
        people = json.load(file)
        result = []
        for person in people:
            result.append(People(
                person['id'],
                person['name'],
                person['sex'],
                person['birth_date']
            ))
        return result


def jsonwrite(people,path):
    with open(path, 'w') as file:
        list = []
        for person in people:
            list.append({
                'id': person.id,
                'name': person.name,
                'sex' : person.sex,
                'birth_date' : person.birth_date
            })
        json.dump(list, file, indent=4)
        


plik = File('people.txt')  
people = plik.create_objects() 


path1 = '/home/pipr/zadania/json_function'
path2 = '/home/pipr/zadania/csv_function'


csvwrite(path1,path2)





        

