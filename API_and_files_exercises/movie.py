import csv
import os
import argparse

class Movie:
    def __init__(self,title,year,genre,duration,description):
        self.title = title
        self.year = year
        self.genre = genre
        self.duration = duration
        self.description = description

    def __str__(self):
        q = f'\nTitle - {self.title}\nYear - {self.year}\nGenre - {self.genre}\nDuration - {self.duration}\nDescription - {self.description}\n'
        return q

class MovieLibrary:
    def __init__(self,path,check_duplicate=False,directory=False):
        self.movie_dict = {}
        self.check_duplicate = check_duplicate
        self.directory = directory
        if directory:
            self.load_directory(path)
        elif directory == False:
            self.load_file(path)    
        else:
            raise ValueError(f'Path Does Not Exist : {path}')    

    def search_movie(self,title):
        if title.lower() in self.movie_dict:
            return self.movie_dict[title.lower()]
        else:
            return(f'no film found\n')


    def load_file(self,path):
        with open(path,'r') as file:
            reader = csv.DictReader(file)
            for line in reader:
                movie = Movie(line.get('title'),line.get('year'),line.get('genre'),line.get('duration'),line.get('description'))
                if self.check_duplicate and movie.title.lower() in self.movie_dict:
                    print(f'*duplicate of {movie.title} found')
                self.movie_dict[movie.title.lower()] = movie

    def load_directory(self,path):
        files = os.listdir(path)
        for file in files:
            file_path = path + '/' + file
            self.load_file(file_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r',action='store_true',required=False)
    parser.add_argument('-d',action='store_true',required=False)
    parser.add_argument('path',type=str)
    args = parser.parse_args()
    library = MovieLibrary(args.path,args.d,args.r)
    while True:
        nazwa = input('Podaj nazwe filmu... ') 
        print(library.search_movie(nazwa))



