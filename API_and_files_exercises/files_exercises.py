import json
import csv


class File():
    def __init__(self, name='', directory='', lines=0):
        self.name = name
        self.directory = directory
        self.lines = lines

    def load(self, path):
        elements = path.split('/')
        self.name = elements[-1]
        self.directory = elements[-2]
        self.lines = len(open(path).readlines())

    def __repr__(self):
        return f'{self.directory}/{self.name}: {self.lines}'


file1 = File()
file1.load('/home/tototmek/PIPR/test_file')

file2 = File()
file2.load('/home/tototmek/PIPR/test_file2')

file3 = File()
file3.load('/home/tototmek/PIPR/test_file3')

files = [file1, file2, file3]


def save_json(files, path):
    with open(path, 'w') as file:
        list = []
        for element in files:
            list.append({
                'name': element.name,
                'directory': element.directory,
                'lines': element.lines
            })
        json.dump(list, file, indent=4)


def load_json(path):
    with open(path, 'r') as file:
        list = json.load(file)
        result = []
        for element in list:
            result.append(File(
                element.get('name'),
                element.get('directory'),
                element.get('lines')
            ))
        return result


def save_csv(data, path):
    with open(path, 'w') as file:
        fieldnames = ['name', 'directory', 'lines']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        for element in data:
            dict = {
                'name': element.name,
                'directory': element.directory,
                'lines': element.lines
            }
            writer.writerow(dict)


def load_csv(path):
    with open(path, 'r') as file:
        reader = csv.DictReader(file)
        result = []
        for element in reader:
            result.append(File(
                element.get('name'),
                element.get('directory'),
                element.get('lines')
            ))
        return result
