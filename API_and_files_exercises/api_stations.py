from matplotlib import pyplot as plt
from random import choice
import requests


def get_stations():
    stations = requests.get('http://api.gios.gov.pl/pjp-api/rest/station/findAll').json()
    all_stations = []
    for station_data in stations:
        station = Station(station_data)
        all_stations.append(station)
    return all_stations

class AirApiObject:
    def __init__(self,data):   
        self._data = data

    def id(self):
        return self._data['id']

class Station(AirApiObject):
    def __init__(self,data):
        super().__init__(data)

    def name(self):
        return self._data['stationName']    

    def pos(self):
        lat = float(self._data['gegrLat'])    
        lon = float(self._data['gegrLon'])
        return (lat,lon)

    def city_name(self):
        return self._data['city']['name']

    def city_data(self):
        return self._data['city']  

    def sensors(self):
        all_sensors = requests.get(f'http://api.gios.gov.pl/pjp-api/rest/station/sensors/{self.id()}').json()
        sensors = []
        for sensor in all_sensors:
            sensor = Sensor(self,sensor)
            sensors.append(sensor)
        return sensors    

    def __str__(self):
        return self.name()    


class Sensor(AirApiObject):
    def __init__(self,station,data):
        super().__init__(data)
        self._station = station

    def name(self):
        return self._data['param']['paramName']

    def station(self):
        return self._station

    def code(self):
        return self._data['param']['paramCode']   

    def param_data(self):
        return self._data['param']   

    def readings(self):
        result = requests.get(f'http://api.gios.gov.pl/pjp-api/rest/data/getData/{self.id()}').json()
        key = result['key']
        values = result['values']
        return [Reading(value['date'],self,key,value['value']) for value in values]

class Reading:
    def __init__(self,date,sensor,key,value):
        self._sensor = sensor  
        self.key = key
        self.value = value
        self.date = date

    def sensor(self):
        return self._sensor   

    def __str__(self):
        return f'{self.key}:{self.value} on {self.date}'         


def main():
    all_stations =  get_stations() 
    station  = choice(all_stations) 
    all_sensors = station.sensors()
    for sensor in all_sensors:
        readings = sensor.readings()
        keys = [reading.date for reading in readings]
        value = [reading.value for reading in readings]
        plt.plot(keys,value,label=sensor.code())
    plt.legend()
    plt.title(label=station.name())
    plt.xticks(rotation=30,fontsize='xx-small',horizontalalignment='right')
    plt.savefig(f'{station}.png')
    plt.show()


main()    
