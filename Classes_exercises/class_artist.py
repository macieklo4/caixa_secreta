class Artist:
    def __init__(self,surname,birth_date):
        self.surname = surname
        self.birth_date = birth_date
        
    def opis(self):
        return self.surname + " was born in " + str(self.birth_date)

class Song:
    def __init__(self,author,title,time) :
        self.author = author
        self.title = title
        self.time = time       

    def characteristics(self):
        return self.author.opis() + " wrote " + self.title + " and it lasts " + str(self.time) + " minutes"  

LucaStrignoli = Artist("Strignoli",1991)
print(LucaStrignoli.opis())      

BlackSmokeRising = Song(LucaStrignoli,"Black Smoke Rising",3.4)
print(BlackSmokeRising.characteristics())