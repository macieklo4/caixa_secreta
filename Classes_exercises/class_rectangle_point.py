def contains(a,b,c,d):
    if (a >= c and a <= d) or (b >= c and b <= d) or (a <= c and a >= d) or (b <= c and b >= d):
        return True
    else:
        return False    
def point_cotains(a,b,c):
    if (a >= c and a <= b) or (a <= c and a >= b):
        return True
    else:
        return False    


class Point:
    def __init__(self,coord_x,coord_y):
        self.coord_x = coord_x
        self.coord_y = coord_y

class Rectangle:
    def __init__(self,top_left,bottom_right):
        self.top_left = top_left
        self.bottom_right = bottom_right

    def intersects(self,rectangle_other):
        warunek1 = contains(self.top_left.coord_x, self.bottom_right.coord_x, rectangle_other.top_left.coord_x, rectangle_other.bottom_right.coord_x)
        warunek2 = contains(rectangle_other.top_left.coord_x, rectangle_other.bottom_right.coord_x,self.top_left.coord_x, self.bottom_right.coord_x)
        warunek3 = contains(self.top_left.coord_y, self.bottom_right.coord_y, rectangle_other.top_left.coord_y, rectangle_other.bottom_right.coord_y)
        warunek4 = contains(rectangle_other.top_left.coord_y, rectangle_other.bottom_right.coord_y,self.top_left.coord_y, self.bottom_right.coord_y)
        return (warunek1 or warunek2) and (warunek3 or warunek4)

    def point_inside(self,point):
        warunek1 = point_cotains(point.coord_x,self.top_left.coord_x,self.bottom_right.coord_x)
        warunek2 = point_cotains(point.coord_y,self.top_left.coord_y,self.bottom_right.coord_y)
        return (warunek1 and warunek2)

    def allarea(self,rectangle_other):
        coordinates_x = (self.top_left.coord_x,self.bottom_right.coord_x,rectangle_other.top_left.coord_x,rectangle_other.bottom_right.coord_x)
        coordinates_y = (self.top_left.coord_y,self.bottom_right.coord_y,rectangle_other.top_left.coord_y,rectangle_other.bottom_right.coord_y)
        minimum_x = min(coordinates_x)
        maximum_x = max(coordinates_x)
        minimum_y = min(coordinates_y)
        maximum_y = max(coordinates_y)
        top_left = (minimum_x,maximum_y)
        bottom_right = (maximum_x,minimum_y)
        return (top_left,bottom_right)

    def point_allarea(self,point):
        coordinates_x = (self.top_left.coord_x,self.bottom_right.coord_x,point.coord_x)
        coordinates_y = (self.top_left.coord_y,self.bottom_right.coord_y,point.coord_y)
        minimum_x = min(coordinates_x)
        maximum_x = max(coordinates_x)
        minimum_y = min(coordinates_y)
        maximum_y = max(coordinates_y)
        top_left = (minimum_x,maximum_y)
        bottom_right = (maximum_x,minimum_y)
        return (top_left,bottom_right)

    def allcoords(self):
        bl_x = self.top_left.coord_x
        bl_y = self.bottom_right.coord_y
        tr_x = self.bottom_right.coord_x
        tr_y = self.top_left.coord_y
        return ((bl_x,bl_y),(tr_x,tr_y))

topleft = Point(2,4)
botright = Point(5,2)
prostokat1 = Rectangle(topleft,botright)
print(prostokat1.allcoords())