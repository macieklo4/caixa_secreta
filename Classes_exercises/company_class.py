class Company: 
    def __init__(self,name,branches=[]): 
        self.name = name 
        self.branches = branches 

    def info(self):
        final = ''
        for branch in self.branches:
            final += branch.__str__() + '\n'
        final = final[0:-1]
        return final    


class Branch(Company): 
    def __init__(self,city=None,surface=None,rooms=None,number=None): 
        if not city or city != str(city):
            raise ValueError('Wrong City Name')
        self.city =  city 
        if not surface or surface <= 0: 
            raise ValueError('Wrong Value') 
        self.surface = surface 
        if not rooms or rooms <= 0: 
            raise ValueError('Wrong Value') 
        self.rooms = rooms 
        if not number or number < 0:
            raise ValueError('Wrong Value') 
        self.number = number 

    def get_city(self):
        return self.city

    def set_city(self,new_city):
        self.city = new_city

    def get_surface(self):
        return self.surface

    def set_surface(self,new_surface):
        self.surface = new_surface

    def get_rooms(self):
        return self.rooms

    def set_rooms(self,new_rooms):
        self.rooms = new_rooms

    def get_number(self):
        return self.number

    def set_number(self,new_number):
        self.number = new_number    

    def __str__(self):
        return f'{self.city},{self.surface},{self.rooms},{self.number}' 
                           

    def max_value(self): 
        max_people_surface = self.surface//15 
        max_people_rooms = 2*self.rooms 
        return min(max_people_rooms,max_people_surface) 
            
    def savings(self):
        dic = {'Warszawa':100,'Krakow':70,'Poznan':45,'Bialystok':30} 
        return self.surface*dic[self.city] 

