class Polynomial:
    def __init__(self, terms=None):
        self.terms = {}
        terms = terms if terms else []
        for degree, coef in terms:
            if coef != 0:
                self.terms[degree] = coef

    def degree(self):
        return max(self.terms) if self.terms else 0

    def coefficient(self, degree):
        return self.terms.get(degree, 0)

    def value(self, x):
        return sum([coef * x ** degree for degree, coef in self.terms.items()])

    def add(self, other_polynomial):
        result_terms = {**self.terms, **other_polynomial.terms}
        for degree, coef in result_terms.items():
            if degree in self.terms and degree in other_polynomial.terms:
                result_terms[degree] = coef + self.terms[degree]
        return Polynomial(list(result_terms.items()))

    def subtract(self, other_polynomial):
        return self.add(Polynomial(list({deg: -coef for deg, coef in other_polynomial.terms.items()}.items())))

    def __str__(self):
        result = ''
        sorted_terms = sorted(self.terms.items(), reverse=True)
        for degree, coef in sorted_terms:
            result += ('+' if coef > 0 else '-') + (str(abs(coef)) if abs(coef) != 1 else '')
            if degree > 1:
                result += f"x^{degree}"
            if degree == 1:
                result += "x"
            if result == "":
                return "0"
        return result[1:] if result[0] == "+" else result