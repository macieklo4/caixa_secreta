import math


class Planeta:
    def __init__(self,moons,x,y,z,name = ""):
        self.moons = moons
        self.x = x
        self.y = y
        self.z = z
        self.name = name

    def distance(self,planet_two):
        distance_planets = math.sqrt((self.x - planet_two.x)**2 + (self.y - planet_two.y)**2 + (self.z - planet_two.z)**2)
        return distance_planets  

    def dajtekst(self):
        return "nazywam sie " + self.name +" i " + "mam " + str(self.moons) + " księzyców i położenie"+ " "+ str(self.x) + "," + str(self.y) + "," + str(self.z)


def distance(planet_one,planet_two):
    distance_planets = math.sqrt((planet_one.x - planet_two.x)**2 + (planet_one.y - planet_two.y)**2 + (planet_one.z - planet_two.z)**2)
    return distance_planets    


Venus = Planeta(3,234,522,5245,"Venus")
Uran = Planeta(2,23423,3413,123,"Uran")
print(distance(Venus,Uran))

print(Venus.distance(Uran))



Ziemia = Planeta(1,234,456453,2344423,"Ziemia") 
print(Ziemia.dajtekst())       



