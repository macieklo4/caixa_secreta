import math
import random
import time

scope = 9


class Imaginary:
    def __init__(self, real=None, abstract=None):
        if not real and not abstract and real != 0:
            raise ValueError('No given number')
        self.real = real
        if not abstract and abstract != 0:
            raise ValueError('No given imaginary_number')
        self.abstract = abstract

    def add(self, other):
        return Imaginary(self.real+other.real, self.abstract+other.abstract)

    def multiply(self, other):
        return Imaginary(self.real*other.real-self.abstract*other.abstract, self.real*other.abstract+self.abstract*other.real)


def terminal_view(a, b, c):
    print(f'{a}^2 + {b}^2 = {c}^2')


def double_check(a, b, c):
    if a**2 + b**2 == c**2:
        return True
    else:
        return False


def get_trio():
    max_c = 0
    start = time.time()
    memory = list()
    i = 1
    for each_real in range(i, scope+1):
        for each_imaginary in range(1, scope+1):
            if each_real != each_imaginary:
                number = Imaginary(each_real, each_imaginary)
                num_squared = number.multiply(number)
                a = abs(num_squared.real)
                b = abs(num_squared.abstract)
                c = each_real**2 + each_imaginary**2
                combination = (a, b, c)
                new_combination = sorted(combination)
                if new_combination not in memory:
                    memory.append(new_combination)
                if c > max_c:
                    max_c = c    
        i += 1
    new_memory = sorted(memory)
    for element in new_memory:
        a, b, c = element
        terminal_view(a, b, c)
    end = time.time()    
    execute_time = end - start
    print(execute_time)
    print(max_c)


if '__main__' == __name__:
    get_trio()
