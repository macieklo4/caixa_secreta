

class Polynomial:
    def __init__(self, values=[]):
        self.values = sorted(values, key=lambda krotka: krotka[0], reverse=True)
        value_previous = None
        for element in self.values:
            if element[1] == 0 or element[0] < 0 or element[0] == value_previous:
                raise ValueError
            value_previous = element[0]    

    def degree(self):
        return self.values[0][0]

    def coefficient(self, degree):
        for element in self.values:
            if element[0] == degree:
                return element[1]
        return 0

    def value(self,value):
        end = 0
        for element in self.values:
            end += element[1]*(value**element[0])
        return end    

    def description(self):
        end = ""
        final = ""
        for element in self.values:
            if element[0] > 0:
                end += str(element[1]) + "x^" + str(element[0]) + "+"  
            else:
                end += str(element[1]) 
        final = end 
        if end[-1] == "+":
            final = end[0:len(end)-1]  
        return final    


    def add(self,polynomial_other):
        i = j = 0
        tab = []
        while i < len(self.values) and j < len(polynomial_other.values):
            if self.values[i][0] > polynomial_other.values[j][0]:
                tab.append(self.values[i])
                i += 1
            elif self.values[i][0] == polynomial_other.values[j][0]:  
                if self.values[i][1] + polynomial_other.values[j][1] == 0:
                    pass
                else:
                    tab.append((self.values[i][0],self.values[i][1] + polynomial_other.values[j][1]))
                i += 1
                j += 1
            elif self.values[i][0] < polynomial_other.values[j][0]:
                tab.append(polynomial_other.values[j])
                j += 1
        if j == len(polynomial_other.values):
            tab += self.values[i::]
        elif i == len(self.values):
            tab += polynomial_other.values[j::]            
        return Polynomial(tab)

    def subtract(self,polynomial_other):
        i = j = 0
        tab = []
        while i < len(self.values) and j < len(polynomial_other.values):
            if self.values[i][0] > polynomial_other.values[j][0]:
                tab.append(self.values[i])
                i += 1
            elif self.values[i][0] == polynomial_other.values[j][0]:
                if self.values[i][1] - polynomial_other.values[j][1] == 0:
                    pass
                else:  
                    tab.append((self.values[i][0],self.values[i][1] - polynomial_other.values[j][1]))
                i += 1
                j += 1
            elif self.values[i][0] < polynomial_other.values[j][0]:
                tab.append((polynomial_other.values[j][0],-polynomial_other.values[j][1]))
                j += 1
        if j == len(polynomial_other.values):
            tab += self.values[i::]
        elif i == len(self.values):
            for krotka in polynomial_other.values[j::]:
                tab.append((krotka[0],-krotka[1]))            
        return Polynomial(tab)
 

        


wielomian1 = Polynomial([(5,3), (2, 2), (1,-7)])
wielomian2 = Polynomial([(6, 1),(5,2),(2,5),(1,7)])
w3 = wielomian1.add(wielomian2)
print(w3.description())

wielomian4 = Polynomial([(0,-8)])
wielomian5 = Polynomial([(3, 1), (0, -7)])
print(wielomian5.description())
print(wielomian4.description())