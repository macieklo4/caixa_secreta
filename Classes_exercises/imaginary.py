import math

class Imaginary:
    def __init__(self,real=None,abstract=None):
        if not real and not abstract and real != 0:
            raise ValueError('No given number')
        self.real = real
        if not abstract and abstract != 0:
            raise ValueError('No given imaginary_number')
        self.abstract = abstract

    def add(self,other):
        return Imaginary(self.real+other.real,self.abstract+other.abstract)

    def subtract(self,other):
        return Imaginary(self.real-other.real,self.abstract-other.abstract)  

    def multiply(self,other):
        return Imaginary(self.real*other.real-self.abstract*other.abstract,self.real*other.abstract+self.abstract*other.real)

    def divide(self,other):
        first = (self.real*other.real+other.abstract*self.abstract)/(other.real**2+other.abstract**2)
        second = (other.real*self.abstract-other.abstract*self.real)/(other.real**2+other.abstract**2) 
        return Imaginary(first,second)  

    def conjugation(self):
        return Imaginary(self.real,-self.abstract) 

    def module(self):
        return math.sqrt(self.real**2 + self.abstract**2)

    def __str__(self):
        if self.real == int(self.real):
            self.real = int(self.real)
        if self.abstract == int(self.abstract):
            self.abstract = int(self.abstract)  

        if self.abstract > 0:
            if self.abstract != 1:
                return f'{self.real}+{self.abstract}i' 
            else: 
                return f'{self.real}+i'

        elif self.abstract < 0:
            if self.abstract != 1:
                return f'{self.real}{self.abstract}i' 
            else:
                return '{self.real}i'    

        else:
            return f'{self.real}'           


num_1 = Imaginary(1,13)  
num_2 = Imaginary(6,2)  
num_3 = num_1.divide(num_2)
print(num_3)

