import random

class Casino:
    def __init__(self,players):
        self.players = players

    def __str__(self):    
        final = ''
        for element in self.players:
            final += element.__str__() + '\n'
        return final[0:-1]    

    def remove(self,player):
        if player in self.players:
            self.players.remove(player)

    def add(self,player):
        if player not in self.players:
            self.players.append(player)        

    def winner(self):
        so_players = []
        for player in self.players:
            so_players.append((player,player.score()))
        so_players = sorted(so_players,key = lambda krotka:krotka[1] ,reverse = True)  
        if so_players[0][1] == so_players[1][1]:
            return 'draw'  
        return so_players[0][0]

    def throw_dices(self):
        tab = list(range(1,7)) 
        result = list(random.choices(tab,k=4))
        return result

    def game(self):
        for player in self.players:
            dices = self.throw_dices()
            player.set_dices(dices)
        print(self.winner())         
                 
class Player:
    def __init__(self,name):
        self.name = name

    def get_name(self):
        return self.name

    def get_dices(self):
        return self.dices  

    def set_name(self,new_name):
        self.name = new_name   

    def set_dices(self,new_dices):
        for dice in new_dices:
            if dice < 1 or dice > 6 or len(new_dices) != 4 or dice != int(dice):
                raise ValueError('Niepoprawne dane wejsciowe')
        self.dices = new_dices    

    def __str__(self):
        return f'{self.name},{self.dices}'  

    def are_all_odd(self):
        for throw in self.dices:
            if throw%2 == 0:
                return False
        return True       

    def are_all_even(self):
        for throw in self.dices:
            if throw%2 == 1:
                return False
        return True           

    def score(self):
        dic = {}
        suma = 0
        sum_even = 0
        sum_odd = 0
        for throw in self.dices:
            if throw in dic:
                dic[throw] += 1
            else:
                dic[throw] = 1
        for key in dic:
            if dic[key] == 2:
                suma += 2*key
            elif dic[key] == 3:
                suma += 4*key 
            elif dic[key] == 4:
                suma += 6*key   
        if self.are_all_odd() == True:
            sum_odd = sum(self.dices) + 3
        if self.are_all_even() == True:
            sum_even = sum(self.dices) + 2   
        self.score = max(suma,sum_even,sum_odd) 
        return self.score

pla_4 = Player('pablo')
pla_5 = Player('wojtek')
pla_6 = Player('monika')
qwert = [pla_4,pla_5,pla_6]
Lotos = Casino(qwert)
Lotos.game()
